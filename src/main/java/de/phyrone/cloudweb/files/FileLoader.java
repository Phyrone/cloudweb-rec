package de.phyrone.cloudweb.files;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class FileLoader
{
  public static String get(String path)
  {
    try
    {
      InputStream in = FileLoader.class.getResourceAsStream(path);
      BufferedInputStream bis = new BufferedInputStream(in);
      ByteArrayOutputStream buf = new ByteArrayOutputStream();
      int result = bis.read();
      while (result != -1)
      {
        buf.write((byte)result);
        //TEST
        result = bis.read();
      }
      return buf.toString("UTF-8");
    }
    catch (Exception e) {}
    return null;
  }
}
