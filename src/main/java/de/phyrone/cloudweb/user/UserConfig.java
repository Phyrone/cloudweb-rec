package de.phyrone.cloudweb.user;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class UserConfig
{
  public HashMap<String, String> USERS;
  private static UserConfig instance;
  
  public UserConfig()
  {
    this.USERS = new HashMap<String, String>();
    this.USERS.put("root", UserManager.get_SHA_512_SecurePassword("123456", "root"));
  }
  
  public static UserConfig getInstance()
  {
    if (instance == null) {
      instance = fromDefaults();
    }
    return instance;
  }
  
  public static void load(File file)
  {
    instance = fromFile(file);
    if (instance == null) {
      instance = fromDefaults();
    }
  }
  
  public static void load(String file)
  {
    load(new File(file));
  }
  
  private static UserConfig fromDefaults()
  {
    UserConfig config = new UserConfig();
    return config;
  }
  
  public void toFile(String file)
  {
    toFile(new File(file));
  }
  
  public void toFile(File file)
  {
    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    String jsonConfig = gson.toJson(this);
    try
    {
      FileWriter writer = new FileWriter(file);
      writer.write(jsonConfig);
      writer.flush();
      writer.close();
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }
  
  private static UserConfig fromFile(File configFile)
  {
    try
    {
      Gson gson = new GsonBuilder().setPrettyPrinting().create();
      BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(configFile)));
      
      return (UserConfig)gson.fromJson(reader, UserConfig.class);
    }
    catch (FileNotFoundException e) {}
    return null;
  }
  
  public String toString()
  {
    Gson gson = new GsonBuilder().setPrettyPrinting().create();
    return gson.toJson(this);
  }
}
