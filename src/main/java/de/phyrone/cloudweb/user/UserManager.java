package de.phyrone.cloudweb.user;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class UserManager
{
  static final String userconfpath = "modules/CloudWeb/users.json";
  static HashMap<String, String> users = new HashMap<String, String>();
  
  public static void addUser(String user, String passwd, Boolean already_hashed)
  {
    if (!already_hashed.booleanValue()) {
      passwd = get_SHA_512_SecurePassword(passwd, user);
    }
    users.put(user, passwd);
  }
  
  public static void removeUser(String username)
  {
    if (users.containsKey(username)) {
      users.remove(username);
    }
  }
  
  public static Boolean allowed(String username, String passwd)
  {
    if (!users.containsKey(username)) {
      return Boolean.valueOf(false);
    }
    if (get_SHA_512_SecurePassword(passwd, username).equals(users.get(username))) {
      return Boolean.valueOf(true);
    }
    return Boolean.valueOf(false);
  }
  
  public static String get_SHA_512_SecurePassword(String passwordToHash, String salt)
  {
    String generatedPassword = null;
    try
    {
      MessageDigest md = MessageDigest.getInstance("SHA-512");
      md.update(salt.getBytes("UTF-8"));
      byte[] bytes = md.digest(passwordToHash.getBytes("UTF-8"));
      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < bytes.length; i++) {
        sb.append(Integer.toString((bytes[i] & 0xFF) + 256, 16).substring(1));
      }
      generatedPassword = sb.toString();
    }
    catch (NoSuchAlgorithmException e)
    {
      e.printStackTrace();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }
    return generatedPassword;
  }
  
  public static void loadfromConf()
  {
    System.out.println("CloudWeb: Loading Users...");
    UserConfig.load("modules/CloudWeb/users.json");
    users = UserConfig.getInstance().USERS;
    UserConfig.getInstance().toFile("modules/CloudWeb/users.json");
  }
  
  public static void savetoConf()
  {
    System.out.println("CloudWeb: Save UserConfig");
    UserConfig.getInstance().USERS = users;
    UserConfig.getInstance().toFile("modules/CloudWeb/users.json");
  }
}
