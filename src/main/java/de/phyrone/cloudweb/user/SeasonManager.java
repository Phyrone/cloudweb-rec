package de.phyrone.cloudweb.user;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import de.phyrone.cloudweb.WebModule;

public class SeasonManager
{
  static final long maxTime = TimeUnit.MINUTES.toMillis(WebModule.conf.getInt("Login_AFK_Timeout_In_Minutes"));
  static HashMap<String, Long> sea = new HashMap<String, Long>();
  
  public static void setSeason(String id, Boolean on)
  {
    if (on.booleanValue()) {
      sea.put(id, Long.valueOf(System.currentTimeMillis()));
    } else {
      sea.put(id, Long.valueOf(-1L));
    }
  }
  
  public static Boolean isSeason(String id)
  {
    Long seatime = (Long)sea.getOrDefault(id, Long.valueOf(-1L));
    long difference = System.currentTimeMillis() - seatime.longValue();
    if (seatime.longValue() == -1L) {
      return Boolean.valueOf(false);
    }
    if (difference > maxTime)
    {
      sea.remove(id);
      return Boolean.valueOf(false);
    }
    if (difference <= maxTime)
    {
      sea.put(id, Long.valueOf(System.currentTimeMillis()));
      return Boolean.valueOf(true);
    }
    return Boolean.valueOf(false);
  }
}
