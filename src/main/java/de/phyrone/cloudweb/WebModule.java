package de.phyrone.cloudweb;

import de.dytanic.cloudnet.lib.utility.document.Document;
import de.dytanic.cloudnetcore.CloudNet;
import de.dytanic.cloudnetcore.api.CoreModule;
import de.phyrone.cloudweb.user.UserManager;
import de.phyrone.cloudweb.web.WebManger;
import spark.Spark;

public class WebModule
  extends CoreModule
{
  public static String Prefix = "CloudWebModule | ";
  public static Document conf;
  static WebModule inst;
  
  public WebModule()
  {
    inst = this;
  }
  
  public void onLoad()
  {try {
	    conf = new Document();
	    conf.append("Port", 8080);
	    conf.append("Title", "Cloudweb");
	    conf.append("Host", "0.0.0.0");
	    conf.append("Login_AFK_Timeout_In_Minutes", 5);
	    UserManager.loadfromConf();
	    createConfig(conf);
  }catch (Exception e) {
	e.printStackTrace();
}

   
  }
  
  public void onBootstrap()
  {try {
	    String[] aliases = { "cloudnetweb" };
	    CloudNet.getInstance().getCommandManager().registerCommand(new UserCMD(aliases));
	    System.out.println("Cloudweb was Start");
	    conf = loadConfig();
	    WebManger.setup();
  }catch (Exception e) {
	e.printStackTrace();
}

  }
  public void onShutdown()
  {try {
	    UserManager.savetoConf();
	    Spark.stop();
  }catch (Exception e) {
	e.printStackTrace();
}
  }
}
