package de.phyrone.cloudweb.web;

import de.phyrone.cloudweb.WebModule;
import de.phyrone.cloudweb.files.FileLoader;

public class LoginPage {
	public String build() {
		return FileLoader.get("login.html").replace("%webname%", WebModule.conf.getString("Title"));
	}

}
