package de.phyrone.cloudweb.web;

import java.util.Collection;

import de.dytanic.cloudnetcore.CloudNet;
import de.dytanic.cloudnetcore.network.components.MinecraftServer;
import de.phyrone.cloudweb.files.FileLoader;

public class ServersPage {
	private String group;
	public ServersPage(String group) {
		this.group = group;
	}
	public String build() {
		return FileLoader.get("servers.html")
				.replace("<!--%groups%-->", getgroups());
	}
	private CharSequence getgroups() {
		Collection<MinecraftServer> list = CloudNet.getInstance().getServers(group);
		String ret = "";
		for(MinecraftServer server: list) {
			String serv = "<tr class=\"odd gradeX\">";
			serv += "<td><a href=\"/server/"+server.getName()+"\">"+server.getName()+"</a></td>";
			serv += "<td>"+server.getLastServerInfo().getServerState().toString()+"</td>";
			serv += "<td>"+server.getServerInfo().getOnlineCount()+"/"+server.getServerInfo().getMaxPlayers()+"</td>";
			serv += "<td class=\"center\">"+server.getLastServerInfo().getTemplate().getName()+"</td>";
			serv += "</tr>";
			ret += serv;
		}
		return ret;
	}

}
