package de.phyrone.cloudweb.web;

import de.phyrone.cloudweb.WebModule;
import de.phyrone.cloudweb.user.SeasonManager;
import de.phyrone.cloudweb.user.UserManager;
import spark.Session;
import spark.Spark;

public class WebManger {
	@SuppressWarnings("deprecation")
	public static void setup() {
		Spark.setPort(WebModule.conf.getInt("Port"));
		Spark.setIpAddress(WebModule.conf.getString("Host"));
		Spark.externalStaticFileLocation("modules/CloudWeb/files");
		Spark.threadPool(10);
		Spark.notFound((req,res)->{
			res.redirect("/");
			return "<h1>404<h1>";
		});
		Spark.get("/login", (req,res) -> {
			if(SeasonManager.isSeason(req.session().id())) {
				res.redirect("/");
				return null;
			}
			return new LoginPage().build();
		});
		Spark.get("/", (req,res) -> {
			if(!SeasonManager.isSeason(req.session().id())) {
				res.redirect("/login");
				return null;
			}
			return new MainPage().build();
		});
		Spark.get("/servers/:group/", (req,res) -> {
			if(!SeasonManager.isSeason(req.session().id())) {
				res.redirect("/login");
				return null;
			}
			return new ServersPage(req.params(":group")).build();
		});
		Spark.get("/servers/", (req,res)->{
			if(!SeasonManager.isSeason(req.session().id())) {
				res.redirect("/login");
				return null;
			}
			return new GroupsPage().build();
		});
		setupAPI();
	}//Api
	private static void setupAPI() {
		Spark.post("/api/login", (req, res) -> {
			String in = req.body();
			String[] ins = in.split("&");
			String passwd = ins[1].replace("password=", "");
			String username = ins[0].replace("user=", "");
			if (UserManager.allowed(username, passwd).booleanValue()) {
				Session s = req.session(true);
				s.attribute("user", username);
				s.maxInactiveInterval(600);
				SeasonManager.setSeason(s.id(), Boolean.valueOf(true));
				System.out.println("Login: " + username + "@" + req.host());
			} else {
				System.out.println("Login Failed: " + username + "@" + req.host());
			}

			res.redirect("/");
			return null;
		});
		Spark.get("/api/logout", (req, res) -> {
			SeasonManager.setSeason(req.session().id(), false);
			res.redirect("/");
			return null;
		});
	}

}
