package de.phyrone.cloudweb.web;

import de.dytanic.cloudnet.lib.server.ServerGroup;
import de.dytanic.cloudnetcore.CloudNet;
import de.phyrone.cloudweb.WebModule;
import de.phyrone.cloudweb.files.FileLoader;

public class GroupsPage {
	
	public String build() {
		return FileLoader.get("groups.html").replace("%title%", WebModule.conf.getString("Title"))
				.replace("<!--groups-->", getGroups());
	}
	private String getGroups() {
		String ret = "";
		for(ServerGroup group:CloudNet.getInstance().getServerGroups().values()) {
			ret += "<tr class=\\\"odd gradeX\\\">";
			ret += "<td><a href=\"/servers/"+group.getName()+"/\">"+group.getName()+"</a></td>";
			ret += "<td>"+CloudNet.getInstance().getServers(group.getName()).size()+"</td>";
			ret +=	"<td>"+group.getGroupMode()+"</td>";
			ret +=	"<td class=\"center\">"+group.getWrapper().toString()+"</td>";
		}
		return ret;
	}
}
