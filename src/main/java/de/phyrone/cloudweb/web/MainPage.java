package de.phyrone.cloudweb.web;

import de.dytanic.cloudnetcore.CloudNet;
import de.phyrone.cloudweb.files.FileLoader;

public class MainPage {
	public String build() {
		return FileLoader.get("root.html")
				.replace("%server_onlinecount%", String.valueOf(CloudNet.getInstance().getServers().keySet().size()))
				.replace("%proxy_onlinecount%", String.valueOf(CloudNet.getInstance().getProxys().keySet().size()))
				.replace("%wrapper_onlinecount%", String.valueOf(CloudNet.getInstance().getWrappers().keySet().size()))
				.replace("%player_onlinecount%", String.valueOf(CloudNet.getInstance().getNetworkManager().getOnlinePlayers().keySet().size()));
	}

}
