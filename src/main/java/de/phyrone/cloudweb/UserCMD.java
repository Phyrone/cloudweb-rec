package de.phyrone.cloudweb;

import de.dytanic.cloudnet.command.Command;
import de.dytanic.cloudnet.command.CommandSender;
import de.phyrone.cloudweb.user.UserManager;

public class UserCMD extends Command {

	protected UserCMD(String[] aliases) {
		super("cloudweb", "cloudweb.admin", aliases);
	}

	public void onExecuteCommand(CommandSender sender, String[] args) {

	    if (args.length > 0)
	    {
	      try
	      {
	        String cmd = args[0];
	        if (cmd.equalsIgnoreCase("adduser"))
	        {
	          if (args.length == 3)
	          {
	            UserManager.addUser(args[1], args[2], Boolean.valueOf(false));
	            sender.sendMessage(new String[] {WebModule.Prefix + "User Created" });
	            UserManager.savetoConf();
	          }
	          else
	          {
	            sender.sendMessage(new String[] {WebModule.Prefix + "User adduser <user> <password>" });
	          }
	        }
	        else if (cmd.equalsIgnoreCase("reload"))
	        {
	          UserManager.loadfromConf();
	          sender.sendMessage(new String[] {WebModule.Prefix + "Config Reloaded" });
	        }
	        else if ((cmd.equalsIgnoreCase("removeuser")) && 
	          (args.length == 2))
	        {
	          UserManager.removeUser(args[1]);
	          UserManager.savetoConf();
	          sender.sendMessage(new String[] {WebModule.Prefix + "User removed" });
	        }
	      }
	      catch (Exception e)
	      {
	        e.printStackTrace();
	      }
	    }
	    else
	    {
	      sender.sendMessage(new String[] { "user adduser <user> <password>" });
	      sender.sendMessage(new String[] { "user removeuser <user>" });
	      sender.sendMessage(new String[] { "user reload" });
	    }
	  
	}

}
